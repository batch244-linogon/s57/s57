let collection = [];

// Write the queue functions below.

//Answer collection
function print() {
   return collection;
}


//Answer enqueque
function enqueue(element) {
  collection[collection.length] = element;
  return collection;
}


//Answer dequeque
function dequeue() {
   let newCollection = [];

   for (i=0;i<collection.length-1;i++){
      newCollection[i] = collection[i+1];
   }
   return collection = newCollection;
}


//Answer Front
function front() {
    return collection[0];
}

//Answer size
function size() {
   return collection.length;
}

//Answer isEmpty
function isEmpty() {
    return (collection.length !==0)? false : true; 
}

module.exports = {
   collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};

